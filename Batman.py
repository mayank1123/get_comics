from lxml import html
import urllib, urllib2
import datetime  
import os , sys
finished = 1

count = 1
chapter_count = 1
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

def get_image(count , path):	
	try:
		url = "http://www.hellocomic.com/batman-the-dark-knight-returns-10th-anniversary/c" + str(chapter)+"/p"+str(count)
		req = urllib2.Request(url,headers= hdr)
		page = urllib2.urlopen(req)
		tree =  html.document_fromstring(page.read())	
		#img_url = tree.xpath("//*[@id='main']/div/div[1]/div[2]/a/img/@src")[0]
		img_url = tree.xpath("//*[@class='coverIssue']/a/img/@src")[0]

		req = urllib2.Request(img_url,headers= hdr)
		img = urllib2.urlopen(req)
		output = open(os.path.join(path ,str(str(count))),'wb')
		output.write(img.read())
		output.close()
		print img_url
	except: 
			not_finsihed = 0

if __name__ == "__main__" : 
	chapter = 1
	while (chapter<(chapter_count+1)):
		not_finished = 1
		count = 1
		path = "/home/mayank/Desktop/Batman/Chapter" + str(chapter)
		if not os.path.exists(path):
			os.mkdir(path,0755)
		while(not_finished):
			get_image(count,path)
			count = count+1
		chapter = chapter + 1